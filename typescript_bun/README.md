# TypeScript with formatting and test on Bun

[![Open in Gitpod](https://gitpod.io/button/open-in-gitpod.svg)](https://gitpod.io/#WORKDIR=typescript_bun/https://gitlab.com/pinage404/nix-sandboxes)

Or with [Nix](https://nixos.org)

```sh
NIX_CONFIG="extra-experimental-features = flakes nix-command" \
nix flake new --template "gitlab:pinage404/nix-sandboxes#typescript_bun" ./your_new_project_directory
```

---

[Available commands](./maskfile.md)

Or just execute

```sh
mask help
```

---

[Awesome Bun](https://github.com/oven-sh/awesome-bun)

[Awesome Node.JS](https://github.com/sindresorhus/awesome-nodejs#contents) not Bun but still accurate

[Awesome JavaScript](https://github.com/sorrycc/awesome-javascript#readme)
