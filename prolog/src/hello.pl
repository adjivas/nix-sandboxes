:- module(hello,[hello/2, unknown/0]).

:- use_module(library(clpfd)).

unknown.

hello(Expected, unknown) :-
    hello(Expected, "world").

hello(Expected, Name) :-
    string_concat("Hello ", Name, Expected).
