module Tests

open Xunit

[<Fact>]
let ``World`` () =
    Assert.Equal("Hello world!", Service.Say.hello None)

[<Fact>]
let ``Jane`` () =
    Assert.Equal("Hello Jane!", Service.Say.hello (Some "Jane"))
