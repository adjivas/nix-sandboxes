#include "hello.h"
#include "unity.h"

void setUp(void) {}

void tearDown(void) {}

void test_hello_world(void) {
  TEST_ASSERT_EQUAL_STRING("Hello world", hello(""));
}

void test_hello_foo(void) {
  TEST_ASSERT_EQUAL_STRING("Hello foo", hello("foo"));
}

int main(void) {
  UNITY_BEGIN();
  RUN_TEST(test_hello_world);
  RUN_TEST(test_hello_foo);
  return UNITY_END();
}
