#include "hello.h"
#include <string.h>

char *hello(char *name) {
  char output[50] = "Hello ";

  if (strlen(name) == 0) {
    name = "world";
  }

  return strcat(output, name);
}
