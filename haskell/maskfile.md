# Commands

## test

```sh
runhaskell ./**/**.hs
```

### test watch

```sh
watchexec --clear -- $MASK test
```

## lint

```sh
hlint ./**/**.hs
```

## format

```sh
fourmolu --mode inplace ./**/**.hs
```

## update

```bash
set -o errexit
set -o nounset
set -o pipefail

nix flake update
direnv exec . \
    nix develop --command \
        devbox update
```

---

<!-- markdownlint-disable-next-line MD039 MD045 -->
This folder has been setup from the [`nix-sandboxes`'s template ![](https://img.shields.io/gitlab/stars/pinage404/nix-sandboxes?style=social)](https://gitlab.com/pinage404/nix-sandboxes)
